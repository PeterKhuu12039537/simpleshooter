There are 10 problems with this game. Can you fix these problems and write down your solutions?


1. There is no music when game is started

Under BackgroundMusic in the hierarchy, tick the "Play On Awake" checkbox

2. x does not start the game

In the Manager script, under the update method. Add in the following piece of code:

if (Input.GetKey("x"))	{
	GameStart();
}

3. Enemies are not getting killed by the players bullets

Add a collider on the player's bullets so that it can interact with the enemies.

4. Player's ship can go out of the visible screen area

Using the min and max values in the player script, set if statements that the player goes out of the boundaries, it will be set the the respective min/max x/y.

5. Enemy bullets will not kill the player ship

In the player script, in the OnTriggerEnter2D method, the initial if statement has to be changed from Bullet(Enemies) -> Bullet(Enemy)

6. Make the bullets from the player's ship come out faster

In the PlayerBullet prefab, change the speed to whatever

7. The first enemy wave is supped to be 3 ships, but the left one is missing

Drag the prefab to the hierarchy, duplicate a ship and drag that prefab back to the assests. Go to the emitter and drag the new wave onto the first element

8. When an enemy ship is killed, the player should get 100 points not 1

In the enemy script, change it from Manager.Current.AddPoint(hp) to Manager.Current.AddPoint(point)

9. High score is not showing or updating

Add in highScoreGUIText.text = highScore.ToString (); to the Manager update method.

10. Make a creative addition to the game!

Added in two side ships that fire bullets as well. There are no changes to these spaceships besides being smaller and having a different colour

====================

Source code is at https://bitbucket.org/yusufpisan/simpleshooter/


You need to create a "Pull Request" in BitBucket.
See https://confluence.atlassian.com/bitbucket/tutorial-learn-about-pull-requests-in-bitbucket-cloud-774243385.html if necessary.


About Forking
      When you work with another user's public Bitbucket repository, typically you have read access to the code but not write access. This is where the concept of forking comes in. Here's how it works:
      Fork the repository to copy it to your own account.
      Clone the forked repository from Bitbucket to your local system.
      Make changes to the local repository.
      Push the changes to your forked repository on Bitbucket.
      Create a pull request from the original repository you forked to add the changes you made.
      Wait for the repository owner to accept or reject your changes.


When using version control with Unity

You need to make the Meta files visible in Unity
    * Edit >Project Settings > Editor -> Version Control    Visible Meta Files 

Add the .gitignore file
    * http://kleber-swf.com/the-definitive-gitignore-for-unity-projects/ Depending on your operating system, the .gitignore file may not be visible in the Explorer, Finder, etc but you can see it at the DOS and Unix shells.

Make sure your repository is readable by 'yusufpisan'

This project already has visible meta files and a .gitignore file (as well as a visible-gitignore.txt to remind you to always add .gitignore